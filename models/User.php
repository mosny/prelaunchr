<?php

namespace app\models;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }
    public function beforeSave($insert)
    {
      if($this->password){
        $this->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);
      }
      $this->accessToken = 'default';
      return parent::beforeSave($insert);
    }
        
    /**
     * @inheritdoc
     */    
    public static function findIdentity($id){
      return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
      return self::findOne(["accessToken" => $token]);
      
    }
    
    public static function findByUsername($username)
    {
      return self::findOne(["username" => $username]);
    }


    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
      //return $this->password === $password;
      return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }
}
