<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ParticipantForm extends Model
{
    public $email;

    public $participant = null;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email'], 'required'],
            // email has to be a valid email address
            ['email', 'email']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email Adresse',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function save()
    {
      if ($this->validate()) {
        if($participant = Participant::findOne([
          'email' => $this->email
        ])){
          $this->participant = $participant;
          return false;
        }
        $activation_code = $this->mt_rand_str(8);
        $participant = new Participant();
        $participant->email = $this->email;
        $participant->active = 0;
        $participant->auth_token = $activation_code;
        $participant->refer_code = $this->mt_rand_str(8);
        $session = Yii::$app->session;
        $participant->referred_id = 0;
        if($session->isActive){
          if($referred_id = $session->get('referred_id')){
            $participant->referred_id = $referred_id;
            $session->remove('referred_id');
          }
        }
        
        $participant->save();
        
        Yii::$app->mailer->compose('confirm-html', [
            'participant' => $participant
        ])
            ->setTo($this->email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject(Setting::forKey("general_email_subject"))
            ->send();
        
        return true;
      } else {
        return false;
      }
    }
    
    public function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
        for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
        return $s;
    }
}
