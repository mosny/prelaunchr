import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('settings', {path: '/'});
  this.route('users');
  this.route('loading');
  this.route('login');
  this.route('notfound');
});

export default Router;
