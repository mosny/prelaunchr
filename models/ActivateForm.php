<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ActivateForm extends Model
{
    public $email;
    
    public $activation_key;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email', 'activation_key'], 'required'],
            // email has to be a valid email address
            ['email', 'email']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function save()
    {
      if ($this->validate()) {
        $activation_code = $this->mt_rand_str(8);
        $participant = new Participant();
        $participant->email = $this->email;
        $participant->active = false;
        $participant->auth_token = $activation_code;
        $participant->refer_code = $this->mt_rand_str(8);
        $participant->referred_id = 0;
        $participant->save();
        
        
        Yii::$app->mailer->compose()
            ->setTo($this->email)
            ->setFrom("mosny@zyg.li")
            ->setSubject("Hallo")
            ->setTextBody("body".$activation_code)
            ->send();
        
        return true;
      } else {
        return false;
      }
    }
    
    public function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
        for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
        return $s;
    }
}
