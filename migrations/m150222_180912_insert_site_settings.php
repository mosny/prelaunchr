<?php

use yii\db\Schema;
use yii\db\Migration;

class m150222_180912_insert_site_settings extends Migration
{
    public function up()
    {
      $this->insert('settings', [
        'setting_key' => 'general_home_large_brandon',
        'setting_type' => 'string',
        'setting_value' => 'Harry&#8217;s is live',
        'active' => 1
      ]);
      $this->insert('settings', [
        'setting_key' => 'general_home_small_brandon',
        'setting_type' => 'string',
        'setting_value' => 'Respecting the Face and Wallet<br>Since Like...Right Now.',
        'active' => 1
      ]);
      $this->insert('settings', [
        'setting_key' => 'general_home_slogan',
        'setting_type' => 'string',
        'setting_value' => 'Be the first to know',
        'active' => 1
      ]);
      $this->insert('settings', [
        'setting_key' => 'general_home_background',
        'setting_type' => 'string',
        'setting_value' => '/images/home/hero@2x.jpg',
        'active' => 1
      ]);
    }

    public function down()
    {

    }
}
