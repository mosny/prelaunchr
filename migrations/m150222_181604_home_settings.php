<?php

use yii\db\Schema;
use yii\db\Migration;

class m150222_181604_home_settings extends Migration
{
    public function up()
    {
      $this->insert('settings', [
        'setting_key' => 'general_home_style_settings',
        'setting_type' => 'style',
        'setting_value' => json_encode(array("background-size" => "1600px 446px")),
        'active' => 1
      ]);
    }

    public function down()
    {

    }
}
