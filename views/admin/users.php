<?php
use app\models\Setting;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <ul class="nav nav-pills">
        <li class="active"><a href="/admin/useradd">Hinzufügen <span class="badge">+</span></a></li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <table class="table table-striped table-hover ">
        <thead>
          <tr>
            <th>#</th>
            <th>Benutzername</th>
            <th>Passwort</th>
            <th>Aktionen</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($users as $user) { ?>
          <tr>
            <?php $form = ActiveForm::begin(['method' => 'post',
            'action' => Url::to(['admin/users/'.$user->id]),
            'fieldConfig' => [
                'template' => "{input}",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],]); ?>
              <td><?= $user->id ?></td>
              <td><?= $user->username ?></td>
              <td><input name="password" type="password" placeholder="Password"></td>
              <td>
                <button class="btn btn-primary" on="click">Aktualisieren</button>
                <a class="btn btn-danger" href="javascript: window.location.href='/admin/deleteUser/<?=$user->id?>'" data-confirm="Wollen Sie die Benutzer wirklich löschen?" on="click">Löschen</a>
              </td>
             <?php ActiveForm::end(); ?>
          </tr>
          <?php } ?>
        </tbody>
      </table>

    </div>
  </div>
</div>