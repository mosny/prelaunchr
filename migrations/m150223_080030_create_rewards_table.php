<?php
/**
*CREATE TABLE IF NOT EXISTS rewards (
*    id integer NOT NULL AUTO_INCREMENT,
*    count integer unsigned DEFAULT 0 NOT NULL,
*    image character varying(255) DEFAULT '' NOT NULL,
*    html character varying(255),
*    created_at timestamp NOT NULL,
*  PRIMARY KEY (id, count)
*);
*
*/
use yii\db\Schema;
use yii\db\Migration;

class m150223_080030_create_rewards_table extends Migration
{
    public function up()
    {
      $this->createTable('rewards', [
                'id' => 'pk',
                'count' => Schema::TYPE_INTEGER . ' unsigned DEFAULT 0 NOT NULL',
                'image' => Schema::TYPE_STRING. " DEFAULT '' NOT NULL",
                'html' => Schema::TYPE_STRING . '',
                'active' => Schema::TYPE_INTEGER . '(1) unsigned DEFAULT 1 NOT NULL'
            ]);

    }

    public function down()
    {
        $this->dropTable('rewards');
    }
}
