<?php
use yii\web\UrlManager;
use app\models\Setting;
use yii\helpers\Url;

/* @var $this yii\web\View */
$url = Yii::$app->urlManager->createAbsoluteUrl('activate/'.$participant->auth_token);
?>
Hallo,

Sie können Ihren Account durch folgenden Link aktivieren:

<?= $url?>