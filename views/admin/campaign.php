<?php
use app\models\Setting;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<div class="container">
  <div class="row" >
    <div class="col-lg-12">
      <fieldset>
        <legend>Kampagne</legend>
        <?php 
        $setting = Setting::findOne(["setting_key" => "specific_campaign_active"]);
        $form = ActiveForm::begin(['method' => 'post',
        'action' => Url::to(['admin/settings/'.$setting->id]),
        'fieldConfig' => [
            'template' => "{input}",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],]); ?>
      <ul class="nav nav-pills" >
        
        <?php if((int)$setting->setting_value == 1){ ?>
          <input type="hidden" name="setting_value" value="0">
          <li>
            <button type="submit" class="btn btn-danger">Kampagne deaktivieren</button>
          </li>
          <?php }else{?>
          <input type="hidden" name="setting_value" value="1">
            <button type="submit" class="btn btn-success">Kampagne aktivieren</button>
        <?php }?>
        
      </ul>
      <?php ActiveForm::end(); ?>
      
    </fieldset>
    </div>
  </div>
  <div class="row" style="margin-top: 50px;">
    <div class="col-lg-12">
      <fieldset>
        <legend>Aktionen</legend>
      <ul class="nav nav-pills">
        <li class="active"><a href="/admin/rewardsadd">Belohnung hinzufügen <span class="badge">+</span></a></li>
        <li><a href="/admin/rewardsempty">Blohnungen leeren <span class="badge"></span></a></li>
      </ul>
    </fieldset>
    </div>
  </div>
  <div class="row" style="margin-top: 20px;">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <fieldset>
        <legend>Belohnungen</legend>
      <table class="table table-striped table-hover ">
        <thead>
          <tr>
            <th width="3">#</th>
            <th width="2">Anzahl</th>
            <th width="50">Bild</th>
            <th width="200">Text</th>
            <th width="2">Aktiv</th>
            <th width="20">Aktionen</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($rewards as $reward) { ?>
          <tr>
            <?php $form = ActiveForm::begin(['method' => 'post',
            'action' => Url::to(['admin/rewards/'.$reward->id]),
            'fieldConfig' => [
                'template' => "{input}",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],]); ?>
              <td><?= $reward->id ?></td>
              <td><input name="count" type="text" value="<?= $reward->count ?>" size="2"></td>
              <td><?= $reward->image ?></td>
              <td><?= $reward->html ?></td>
              <td><?= $reward->active ?></td>
              <td>
                <button class="btn btn-primary" on="click">Aktualisieren</button>
                <a class="btn btn-danger" href="javascript: window.location.href='/admin/deleteReward/<?=$reward->id?>'" data-confirm="Wollen Sie die Belohnung wirklich löschen?" on="click">Löschen</a>
              </td>
             <?php ActiveForm::end(); ?>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </fieldset>
    </div>
  </div>
</div>