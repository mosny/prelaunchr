<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;
use app\models\Setting;

/* @var $this \yii\web\View */
/* @var $content string */

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="utf-8">
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../bower_components/html5shiv/dist/html5shiv.js"></script>
      <script src="../bower_components/respond/dest/respond.min.js"></script>
    <![endif]-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> <?= Setting::forKey("general_site_title") ?></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <?php $this->head() ?>
  </head>
  <body>
    <?php $this->beginBody() ?>
    <?php if (!\Yii::$app->user->isGuest) { ?>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="/administrate" class="navbar-brand">Administration</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li class="active">
              <a href="/admin">Einstellungen</a>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Kampagne <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a href="/admin/campaign">Einstellungen</a></li>
                <li class="divider"></li>
                <li><a href="/admin/registered">Registrierte Teilnehmer</a></li>
<!--                <li><a href="/admin/stats">Auswertungen</a></li> -->
              </ul>
            </li>

            <li>
              <a href="<?=Url::to(['admin/users'])?>">Administrative Benutzer</a>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?=Url::to(['site/logout'])?>" data-method="post">Abmelden</a></li>
          </ul>

        </div>
      </div>
    </div>

    <?php }?>
    <div class="container" style="margin-top: 90px;">
      <div class="row">
        <?php foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
          ?>
          <div class="alert alert-dismissible alert-<?=$key?>">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <p><?=$message?></p>
          </div>
        
        <?php } ?>
      </div>

  

    <?= $content ?>


  
    </div>
  <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>