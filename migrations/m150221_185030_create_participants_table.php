<?php

use yii\db\Schema;
use yii\db\Migration;

class m150221_185030_create_participants_table extends Migration
{
    public function up()
    {
      $this->createTable('participants', [
                'id' => 'pk',
                'email' => Schema::TYPE_STRING . ' NOT NULL',
                'auth_token' => Schema::TYPE_STRING. '(8) NOT NULL',
                'refer_code' => Schema::TYPE_STRING . ' NOT NULL',
                
                'referred_id' => Schema::TYPE_INTEGER . '(20) unsigned DEFAULT 0 NOT NULL',
                'active' => Schema::TYPE_INTEGER . '(1) unsigned DEFAULT 0 NOT NULL'
                
            ]);
    }

    public function down()
    {
        $this->dropTable('participants');
    }
}
