<?php

use yii\db\Schema;
use yii\db\Migration;

class m150222_175601_insert_more_settings extends Migration
{
  public function up()
  {
    $this->insert('settings', [
      'setting_key' => 'general_site_title',
      'setting_type' => 'string',
      'setting_value' => 'Harry&#8217;s &ndash; Tell Friends, Shave for Free',
      'active' => 1
    ]);
    $this->insert('settings', [
      'setting_key' => 'general_site_description',
      'setting_type' => 'string',
      'setting_value' => 'Shaving is evolving. Didn’t want to leave you all behind. Check out Harry’s here.',
      'active' => 1
    ]);
    $this->insert('settings', [
      'setting_key' => 'general_og_image',
      'setting_type' => 'string',
      'setting_value' => '/assets/refer/truman.png',
      'active' => 1
    ]);
    $this->insert('settings', [
      'setting_key' => 'share_facebook_sharer_prefix',
      'setting_type' => 'string',
      'setting_value' => 'http://www.facebook.com/sharer/sharer.php?u=',
      'active' => 1
    ]);
    $this->insert('settings', [
      'setting_key' => 'share_twitter_sharer_prefix',
      'setting_type' => 'string',
      'setting_value' => 'http://twitter.com/share?url=',
      'active' => 1
    ]);
    $this->insert('settings', [
      'setting_key' => 'specific_campaign_active',
      'setting_type' => 'bool',
      'setting_value' => 'true',
      'active' => 1
    ]); 
       
  }

}
