<?php
use app\models\Setting;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <table class="table table-striped table-hover ">
        <thead>
          <tr>
            <th>#</th>
            <th>Key</th>
            <th>Type</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach (Setting::find()->all() as $setting) { ?>
          <tr>
            <?php $form = ActiveForm::begin(['method' => 'post',
            'action' => Url::to(['admin/settings/'.$setting->id]),
            'fieldConfig' => [
                'template' => "{input}",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],]); ?>
              <input type="hidden" name="setting_key" value="<?= $setting->setting_key ?>">
              <input type="hidden" name="setting_type" value="<?= $setting->setting_type ?>">
              <td><?= $setting->id ?></td>
              <td><?= $setting->setting_key ?></td>
              <td><?= $setting->setting_type ?></td>
              <td><input type="text" name="setting_value" value="<?= \yii\helpers\Html::encode($setting->setting_value) ?>" size=50 /></td>
              <td><button class="btn btn-primary" on="click">save</button></td>
             <?php ActiveForm::end(); ?>
          </tr>
          <?php } ?>
        </tbody>
      </table>

    </div>
  </div>
</div>