<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Setting;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6 home"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7 home"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8 home"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9 home"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="home" lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
            
    <link rel="icon" href="<?= Setting::forKey("general_favicon") ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?= Setting::forKey("general_favicon") ?>" type="image/x-icon">

    <meta property="og:title" content="<?= Html::encode($this->title) ?> | <?= Setting::forKey("general_site_title") ?>"/>
    <meta property="og:description" content="<?= Setting::forKey("general_site_description") ?>"/>
    <meta property="og:image" content="<?= Setting::forKey("general_og_image") ?>"/>
    <meta property="og:url" content="----"/>
    
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> <?= Setting::forKey("general_site_title") ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

  <?= $content ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>