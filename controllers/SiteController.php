<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ParticipantForm;
use app\models\ActivateForm;
use app\models\Participant;
use yii\web\Session;
use app\models\Setting;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'index' => ['post', 'get']
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionInactive()
    {
      if((int)Setting::forKey("specific_campaign_active") == 1)
        return $this->redirect("/");
      return $this->render('inactive');
    }
    
    public function actionIndex()
    {
      if((int)Setting::forKey("specific_campaign_active") == 0)
        return $this->redirect("/inactive");
      
      $session = Yii::$app->session;
      if(!$session->isActive)
        $session->open();
      if($session->get('participant')){
        return $this->redirect("/participants/refer");
      }
      $model = new ParticipantForm();

      if ($model->load(Yii::$app->request->post()) && $model->save()){
        $activation = new ActivateForm();
        return $this->render('activate', [
            'user' => $model,
            'model' =>$activation
        ]);
      }else{
        if($model->participant){
          $session->set('participant', $model->participant);
          return $this->redirect("/participants/refer");
        }
        return $this->render('index', [
            'model' => $model,
        ]);
      }
    }
    public function actionParticipate()
    {
      
    }

    public function actionLogin()
    {
      $this->layout = 'admin';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
