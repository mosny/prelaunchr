<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ParticipantForm;
use app\models\ActivateForm;
use app\models\Participant;
use yii\web\Session;
use yii\web\UrlManager;
use yii\web\Cookie;
use app\models\Setting;

class ParticipantsController extends Controller
{
  public function behaviors()
  {
      return [
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'refer' => ['get'],
                  'activate' => ['get']
              ],
          ],
      ];
  }
  
  public function actionSet()
  {
    $session = Yii::$app->session;
    if(!$session->isActive){
      $session->open();
    }
    $slug = \Yii::$app->request->getQueryParam('slug');
    if(!$slug){
      $session->setFlash('error', 'Es wurde kein Code übermittelt');
      return $this->redirect("/");
    }
    $participant = Participant::findOne([
      'refer_code' => $slug
    ]);
    if(!$participant){
      return $this->redirect("/");
    }
    $session->set("referred_id", $participant->id);
    return $this->redirect("/"); 
  }

    public function actionRefer()
    {
      if(Setting::forKey("specific_campaign_active") == 0)
        return $this->redirect("/inactive");
      
      $session = Yii::$app->session;
      if(!$session->isActive){
        $session->open();
      }
      if($participant = $session->get("participant")){
        $count = Participant::find()
            ->where(['referred_id' => $participant->id])
            ->count();
        return $this->render('refer',[
              'participant' => $participant,
              'refer_count' => $count
          ]);
      }else{
        $session->setFlash('error', 'Ihre Sitzung konnte nicht validiert werden. Bitte geben Sie Ihre E-mail Adresse ein um fortzufahren.');
        return $this->redirect("/");
      }
    }
    
    public function actionActivate()
    {
      $slug = \Yii::$app->request->getQueryParam('slug');
      
      $session = Yii::$app->session;
      if(!$session->isActive)
        $session->open();
      
      $participant = Participant::findOne([
        'auth_token' => $slug
      ]);
      if(!$participant){
        $session->setFlash('error', 'Es konnte keine Benutzerzuordnung gefunden werden. Vielleicht ist Ihr Account bereits aktiviert?');
        
        return $this->redirect("/");
      }
      $participant->active = 1;
      $participant->auth_token = '';
      $participant->update();
      $session->set('participant', $participant);
      //return print_r($slug);
      return $this->redirect("/participants/refer");
    }

}
