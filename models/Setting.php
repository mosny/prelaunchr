<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property string $setting_key
 * @property string $setting_type
 * @property string $setting_value
 * @property integer $active
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['setting_key', 'setting_type', 'setting_value'], 'required'],
            [['active'], 'integer'],
            [['setting_key'], 'string', 'max' => 40],
            [['setting_type'], 'string', 'max' => 8],
            [['setting_value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_key' => Yii::t('app', 'Setting Key'),
            'setting_type' => Yii::t('app', 'Setting Type'),
            'setting_value' => Yii::t('app', 'Setting Value'),
            'active' => Yii::t('app', 'Active'),
        ];
    }
    public function updateOrInsert()
    {
      $res = self::findOne(['setting_key' => $this->setting_key]);
      if($res) $this->setIsNewRecord(false);
      
      $this->save();
    }
    public static function forKey($key)
    {
      $result =  self::findOne([
          'setting_key' => $key
      ]);

      if(!$result)
        return null;
      
      $value = $result->setting_value;
      $type = $result->setting_type;
      switch($type){
        case 'bool':
          return (bool) $value;
          break;
        case 'array':
          return json_decode($value);
          break;
        default:
          return $value;
      }
    }
}
