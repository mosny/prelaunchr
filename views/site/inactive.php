<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Setting;
/* @var $this yii\web\View */
/* @var $model app\models\ActivateForm */
/* @var $form ActiveForm */
$enabled = Setting::forKey("general_startdate_enable");
?>
<body id="home" <?php if($enabled){ ?>onload="openCounter()"<?php } ?>>
    <div class="hero">
        <p class="large brandon">Inaktiv</p>
        <p class="small brandon" id="active-since">Diese Kampagne ist inaktiv.</p>
    </div>
    <?php if($enabled){ ?>
      
      
    <script type="text/javascript">
    function openCounter(){
      $("#active-since")
      .countdown("<?=Setting::forKey("general_startdate")?>", function(event) {
        $(this).text(
          event.strftime('noch %D Tage %H:%M:%S zur Veröffentlichung')
        );
      });
      }
    </script>
    <?php } ?>

</body>
