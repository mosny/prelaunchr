<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'rEectrhXWR6beqlhyBi6zlkdUJJZ0Deu',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'nickcv\mandrill\Mailer',
            'apikey' => 'A0KmwjfY5En4vvKFnyWpNQ',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'baseUrl' => '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
              'inactive' => 'site/inactive',
              'refer/<slug:[a-zA-Z0-9-]+>' => 'participants/set',
              'activate/<slug:[a-zA-Z0-9-]+>' => 'participants/activate',
              'admin/users/<id:[0-9]+>' => 'admin/updateuser',
              
              'admin/settings/<id:[0-9]+>' => 'admin/get',
              'admin/rewards/<id:[0-9]+>' => 'admin/updaterewards',
              'admin/deleteReward/<id:[0-9]+>' => 'admin/deletereward',
              'admin/deleteUser/<id:[0-9]+>' => 'admin/deleteuser',
              'administrate' => 'admin/settings',
               // default controller url setup
               '<controller:w+>/<id:d+>'=>'<controller>/view',
               '<controller:w+>/<action:w+>/<id:d+>'=>'<controller>/<action>',
               '<controller:w+>/<action:w+>'=>'<controller>/<action>',
               
               // defaults to a site page if not above
               '<view:[a-zA-Z0-9-]+>/'=>'site/index',
               ]
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
