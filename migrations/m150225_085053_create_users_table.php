<?php

use yii\db\Schema;
use yii\db\Migration;

class m150225_085053_create_users_table extends Migration
{
    public function up()
    {
      $this->createTable('users', [
                'id' => 'pk',
                'username' => Schema::TYPE_STRING . ' NOT NULL',
                'password' => Schema::TYPE_STRING . ' NOT NULL',
                'authKey' => Schema::TYPE_STRING . '',
                'accessToken' => Schema::TYPE_STRING . ' NOT NULL',
                
            ]);
    }

    public function down()
    {
        $this->dropTable('users');
    }
}
