<?php
use app\models\Setting;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <?php $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n{error}",
                'labelOptions' => ['class' => 'col-lg-2 control-label '],
            ],
        ]); ?>
      <fieldset>
        <legend>Neue Belohnung hinzufügen</legend>
        <div class="form-group">
          <?= $form->field($model, 'count') ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'image') ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'html') ?>
        </div>
        
    
        <div class="form-group">
          <div class="col-lg-10 col-lg-offset-2">
            <button type="reset" class="btn btn-default">Abbrechen</button>
            <button type="submit" class="btn btn-primary">Erstellen</button>
          </div>
        </div>
      </fieldset>


        <?php ActiveForm::end(); ?>

    </div>
  </div>
</div>
