<?php

use yii\db\Schema;
use yii\db\Migration;

class m150221_190158_create_settings extends Migration
{
    public function up()
    {
      $this->createTable('settings', [
                'id' => 'pk',
                'setting_key' => Schema::TYPE_STRING . '(40) NOT NULL',
                'setting_type' => Schema::TYPE_STRING. '(8) NOT NULL',
                'setting_value' => Schema::TYPE_STRING . ' NOT NULL',
                'active' => Schema::TYPE_INTEGER . '(1) unsigned DEFAULT 0 NOT NULL'
            ]);
    }

    public function down()
    {
        $this->dropTable('settings');
    }
}