<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participants".
 *
 * @property integer $id
 * @property string $email
 * @property string $refer_code
 * @property integer $referred_id
 * @property integer $active
 * @property string $auth_token
 */
class Participant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'refer_code'], 'required'],
            [['referred_id', 'active'], 'integer'],
            [['email'], 'string', 'max' => 100],
            [['refer_code'], 'string', 'max' => 20],
            [['auth_token'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'refer_code' => 'Refer Code',
            'referred_id' => 'Referred ID',
            'active' => 'Active',
            'auth_token' => 'Auth Token'
        ];
    }
}
