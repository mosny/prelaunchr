<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rewards".
 *
 * @property integer $id
 * @property integer $count
 * @property string $image
 * @property string $html
 * @property integer $active
 */
class Reward extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rewards';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count', 'active'], 'integer'],
            [['image', 'html'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'count' => Yii::t('app', 'Count'),
            'image' => Yii::t('app', 'Image'),
            'html' => Yii::t('app', 'Html'),
            'active' => Yii::t('app', 'Active'),
        ];
    }
}
