<?php

/**
 * @copyright Copyright (c) 2015 jacor
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Participant;
use yii\web\Session;
use yii\web\UrlManager;
use app\models\User;
use app\models\Setting;
use app\models\Reward;

/**
 * AdminController is the class, that controls reads and writes to parameters of the frontend
 *
 *
 * @author Florian Kasper
 * @since 0.0.3
 */
class AdminController extends Controller
{
  
  /**
   * @inheritdoc
   */
  public $layout = 'admin';

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
      return [
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'get' => ['get', 'post'],
                'useradd' => ['post', 'get'],
                'users' => ['get'],
                'campaign' => ['get'],
                'rewardsadd' => ['get', 'post'],
                'updateuser' => ['post']
            ],
        ],
        'access' => [
             'class' => AccessControl::className(),

             'rules' => [
                 // deny all POST requests
                 [
                     'allow' => false,
                     'verbs' => ['post']
                 ],
                 // allow authenticated users
                 [
                     'allow' => true,
                     'roles' => ['@'],
                 ],
                 // everything else is denied
             ],
         ]
          
      ];
  }
  
  /**
   * Generates /admin/users
   * @method GET
   * @return Render 
   */
  public function actionUsers()
  {
    return $this->render("users",[
      "users" => User::find()->all()
    ]);
  }
  
  /**
   * Generates /admin/useradd
   * @method GET/POST
   * @return Render or Redirect
   */
  public function actionUseradd()
  {
    $model = new User();
    $request = \Yii::$app->request->post("User");
    $model->username = $request["username"];
    $model->password = $request["password"];
    if (Yii::$app->request->getIsPost() && $model->save() ){
      Yii::$app->getSession()->setFlash('success', 'Benutzer wurde erfolgreich angelegt!');
      return $this->redirect('/admin/users');
    }else{
      
      return $this->render("new_user",[
        "model" => $model
      ]);
    } 
  }
  
  /**
   * Generates /admin/user/:id
   * @method POST
   * @return Redirect
   */  
  public function actionUpdateuser()
  {
    $id = \Yii::$app->request->getQueryParam('id');
    $model = User::findOne($id);
    if($id && $model){
      Yii::$app->getSession()->setFlash('success', 'Benutzer wurde erfolgreich aktualisiert!');
      $password = Yii::$app->request->post("password");
      if($password){
        $model->password = $password;
        $model->update();
      }
    }else{
      Yii::$app->getSession()->setFlash('error', 'Benutzer Aktualisierung fehlgeschlagen');
      
    }
    return $this->redirect("/admin/users");
  }
  
  /**
   * Generates /admin/campaign
   * @method GET
   * @return Render 
   */
  public function actionCampaign()
  {
    return $this->render("campaign",[
      "rewards" => Reward::find()->all()
    ]);
  }
  
  /**
   * Generates /admin/rewardsempty
   * @method ANY
   * @return Redirect 
   */
  public function actionRewardsempty()
  {
    \Yii::$app->db->createCommand()->truncateTable(Reward::tableName());
    Yii::$app->getSession()->setFlash('success', 'Alle Belohnungen wurden erfolgreich gelöscht!');
    return $this->redirect("/admin/campaign");
  }
  
  /**
   * Generates /admin/rewardsadd
   * @method GET | POST
   * @return Render Redirect
   */
  public function actionRewardsadd()
  {
    $model = new Reward();
    if ($model->load(Yii::$app->request->post()) && $model->save()){
      Yii::$app->getSession()->setFlash('success', 'Belohnung wurde erfolgreich angelegt!');
      return $this->redirect('/admin/campaign');
    }else{
      return $this->render("new_reward",[
        "model" => $model
      ]);
    }
  }
  
  /**
   * Generates /admin/deletereward
   * @method GET
   * @return Redirect 
   */
  public function actionDeletereward()
  {
    $id = \Yii::$app->request->getQueryParam('id');
    $model = Reward::findOne($id);
    
    if($id && $model){
      $model->delete();
      Yii::$app->getSession()->setFlash('success', 'Belohnung wurde erfolgreich gelöscht!');
    }else{
      Yii::$app->getSession()->setFlash('error', 'Fehler beim Löschen');
    }
    return $this->redirect('/admin/campaign');
  } 
  
  /**
   * Generates /admin/deleteuser
   * @method GET
   * @return Redirect 
   */
  public function actionDeleteuser()
  {
    $id = \Yii::$app->request->getQueryParam('id');
    $model = User::findOne($id);
    
    if($id && $model){
      $model->delete();
      Yii::$app->getSession()->setFlash('success', 'Benutzer wurde erfolgreich gelöscht!');
    }else{
      Yii::$app->getSession()->setFlash('error', 'Fehler beim Löschen des Benutzer');
    }
    return $this->redirect('/admin/users');
  }
   
  /**
   * Generates /admin/settings/:id
   * @method POST
   * @return Redirect 
   */
  public function actionGet()
  {
    $id = \Yii::$app->request->getQueryParam('id') ;
    
    $setting = Setting::findOne($id);
    $setting->setting_value = \Yii::$app->request->post("setting_value");

    if(!$setting->update()){
      Yii::$app->getSession()->setFlash('error', 'Fehler beim Aktualisieren.');
    }
    return $this->goBack();
  }
  
  /**
   * Generates /administer
   * @method GET
   * @return Render 
   */
  public function actionSettings()
  {
    $model = new Setting();

    return $this->render("settings");// array("jsettings" => Setting::find()->all());
  }

  /**
  *
  */
  public function actionRegistered()
  {
    $countGlob = Participant::find()
        ->count();
    $count24 = Participant::find()
        ->count();
    $count1 = Participant::find()
        ->count();
    $countWeek = Participant::find()
        ->count();
    $countMonth = Participant::find()
        ->count();
    $activatedCount = Participant::find()
        ->where(['active' => "1"])
        ->count();
    return $this->render("registered",[
      "registeredCount" => $countGlob,
      "last24" => $count24,
      "last1" =>$count1,
      "lastWeek" => $countWeek,
      "lastMonth" => $countMonth,
      "activatedCount" => $activatedCount
    ]);
  }
  public function render_403()
  {
    \Yii::$app->response->setStatusCode(403, "Not Authorized");
    return $this->redirect("/admin/login");
  }
}
