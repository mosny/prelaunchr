<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-10\">{input}</div>\n{error}",
            'labelOptions' => ['class' => 'col-lg-2 control-label '],
        ],
    ]); ?>
  <fieldset>
    <legend>Login</legend>
    <div class="form-group">
      <?= $form->field($model, 'username') ?>
    </div>
    <div class="form-group">
       <?= $form->field($model, 'password')->passwordInput() ?>
    </div>
    
    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
        <button type="reset" class="btn btn-default">Abbrechen</button>
        <button type="submit" class="btn btn-primary">Anmelden</button>
      </div>
    </div>
  </fieldset>


    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
