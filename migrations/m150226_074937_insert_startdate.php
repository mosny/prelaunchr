<?php

use yii\db\Schema;
use yii\db\Migration;

class m150226_074937_insert_startdate extends Migration
{
    public function up()
    {
      $this->insert('settings', [
        'setting_key' => 'general_startdate_enable',
        'setting_type' => 'bool',
        'setting_value' => 'true',
        'active' => 1
      ]);
      $this->insert('settings', [
        'setting_key' => 'general_startdate',
        'setting_type' => 'string',
        'setting_value' => '2015/03/03',
        'active' => 1
      ]);
    }

    public function down()
    {

    }
}
