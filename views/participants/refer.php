<?php
use yii\web\UrlManager;
use app\models\Setting;
use yii\helpers\Url;
use app\models\Reward;
/* @var $this yii\web\View */
$url = Yii::$app->urlManager->createAbsoluteUrl('refer/'.$participant->refer_code);
$width = 165;
?>
<div id="refer">

<div class="header">
    <div class="content">
        <p class="logo brandon">Harry&#8217;s</p>
        <p class="byline brandon">THANK YOU FOR SIGNING UP</p>
    </div>
</div>

<div class="hero">
    <div class="page-content clearfix">
        <div class="mammoth">
            <span class="need brandon">Shaving Is<br>Evolving</span>
        </div>
        <div class="share-wrap">
            <p class="why brandon">Don't leave your friends behind</p>
            <p class="title brandon">INVITE FRIENDS &<br>Earn Product</p>
            <p class="subtitle brandon">Share your unique link via email, Facebook<br>or Twitter and earn Harry's goods for<br>each friend who signs up.</p>
            <div class="copy-link brandon"><?=$url?></div>
            <div class="social-links">
                <a href="http://www.facebook.com/sharer/sharer.php?u=<?=$url?>" class="fb" target="_blank"></a>
                <div class="sep"></div>
                <a href="http://twitter.com/share?url=<?=$url?>&text=%23Shaving+is+evolving.+Excited+for+%40harrys+to+launch." class="twit" target="_blank"></a>
            </div>
        </div>
    </div>
</div>

<div class="prizes ">
    <div class="page-content">
        <p class="callout brandon">Here's How It Works:</p>
        <ul class="products clearfix">
          <li class="title brandon">
            <p class="friends">Friends Joined</p>
            <p class="rewards">Harry's Product</p>
          </li>
          <?php foreach (Reward::find()->all() as $reward) { ?>
              <li class="product brandon<?php if($refer_count >= $reward->count) {?> selected<?php }?>">
                  <div class="circle"><?= $reward->count ?></div>

                  <div class="sep"></div>
                  <p><?= $reward->html ?></p>

                  <div class="tooltip">
                      <img src="<?= $reward->image ?>" height="254">
                  </div>
              </li>
              
          <?php 
            if($refer_count >= $reward->count) {
              $width = $width + 254;
            }
          }
          ?>
        </ul>

        <div class="progress">
            <div class="mover" >
                <div class="bar clearfix" style="width: <?= $width ?>px"></div>
            </div>
        </div>


            <p class="place brandon"><span>No</span> friends have joined...Yet!</p>
        <p class="check brandon">Keep checking</p>

        <p class="ship brandon">We ship to the U.S. (& Canada Soon)</p>        
        <a class="policy brandon" href="/privacy-policy" target="_blank">Privacy Policy</a>
    </div>
</div>


</div>