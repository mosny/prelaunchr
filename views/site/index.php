<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Setting;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = '';
?>
<body id="home">
    <div class="hero" style="background: url(<?= Setting::forKey("general_home_background") ?>) center center no-repeat;<?= Setting::forKey("general_home_style_settings") ?>)">
        <p class="large brandon"><?= Setting::forKey("general_home_large_brandon") ?></p>
        <p class="small brandon"><?= Setting::forKey("general_home_small_brandon") ?></p>
    </div>

    <div class="form-wrap clearfix ">
      <div class="key"></div>
      <p class="byline brandon"><?= Setting::forKey("general_home_slogan") ?></p>
        <div style="margin:0;padding:0;display:inline">
          <?php $form = ActiveForm::begin(['method' => 'post','fieldConfig' => [
              'template' => "{input}",
              'labelOptions' => ['class' => 'col-lg-1 control-label'],
          ],]); ?>
          
          <?= Html::activeTextInput($model, 'email',array('placeholder' =>"Enter Email",'size'=>30,'class'=>'brandon email')); ?>

          <input class="submit brandon" name="commit" type="submit" value="Step Inside" />
           <?= Yii::$app->session->getFlash('error'); ?>
          <?php ActiveForm::end(); ?>

    </div>

</body>
