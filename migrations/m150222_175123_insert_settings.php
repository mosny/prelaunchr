<?php

use yii\db\Schema;
use yii\db\Migration;

class m150222_175123_insert_settings extends Migration
{
    public function up()
    {
      $this->insert('settings', [
        'setting_key' => 'general_favicon',
        'setting_type' => 'string',
        'setting_value' => '/assets/favicon.ico',
        'active' => 1
      ]);
    }

    public function down()
    {
      
    }
}
