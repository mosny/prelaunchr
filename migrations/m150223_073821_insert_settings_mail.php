<?php

use yii\db\Schema;
use yii\db\Migration;

class m150223_073821_insert_settings_mail extends Migration
{
    public function up()
    {
      $this->insert('settings', [
        'setting_key' => 'general_mandrill_appkey',
        'setting_type' => 'string',
        'setting_value' => 'A0KmwjfY5En4vvKFnyWpNQ',
        'active' => 1
      ]);
      $this->insert('settings', [
        'setting_key' => 'general_email_subject',
        'setting_type' => 'string',
        'setting_value' => 'Bestätigen Sie Ihre Email Adresse',
        'active' => 1
      ]);
    }

    public function down()
    {

    }
}
